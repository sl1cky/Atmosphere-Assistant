# Atmosphere Assistant
A Discord bot for the *soon* arriving Atmosphere server network. Coded and intended to fit the exact needs of Atmosphere.  

*Also, this bot is most likely not going to be maintained as frequently as the [Matrix](https://matrix.org/) bot I'll be making, once I get to coding that one.*  

## Features
Atmosphere Assistant aims to fit all the needs of Atmosphere, in which includes management of:
+ Roles
+ Minigames
+ Admin commands
+ Channels & rooms
