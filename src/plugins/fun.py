import hikari as hk
import lightbulb as lb
from log import logging, Logger
from random import randrange
from datetime import datetime

fun_plugin = lb.Plugin("Fun", description="All the fun commands such as 8ball.")
current_time = datetime.now().astimezone()
Logger()  # Initialize logging config


@fun_plugin.command()
@lb.add_cooldown(3.0, 1, lb.UserBucket)
@lb.option('stop', "What number to stop at (default 10).", type=int, required=False)
@lb.option('start', "What number to start from (default 1).", type=int, required=False)
@lb.option('guess', "Your guess.", type=int, required=False)
@lb.command('number', "Guess or get a random number from 1-10 by default.")
@lb.implements(lb.SlashCommand)
async def number_games(ctx) -> None:
    user = ctx.member.display_name
    usr_num = ctx.options.guess
    start_num = ctx.options.start
    stop_num = ctx.options.stop

    num_range = (1 if start_num is None else start_num, 11 if stop_num is None else stop_num + 1)
    rand_num = randrange(num_range[0], num_range[1])
    won = True if usr_num == rand_num else False
    message = "Winner winner chicken dinner." if won else "Too bad! You didn't guess the number. Big L."

    logging.debug(f"Numbers: {ctx.author} rolled a '{rand_num}'. Guess '{usr_num}'. "
                  f"Range '{start_num, stop_num}'. Actual '{num_range}'.")

    if usr_num is None:
        match rand_num:
            case 7:
                await ctx.respond("The random number is... lucky `7`!")
            case 69:
                await ctx.respond("The random number is... `69`! Noice.")
            case 360:
                await ctx.respond("The random number is... `360`! nO sCoPe.")
            case 420:
                await ctx.respond("The random number is... `420`! Blaze it.")
            case 6969:
                await ctx.respond("The random number is... `6969`! Nice x2.")
            case _:  # When the number is something else than what have been checked for
                await ctx.respond(f"The random number is... `{rand_num}`!")
        return

    embed = (
        hk.Embed(
            title=f"__Number__ - {user}",
            colour=0x00E2E2,
            timestamp=current_time,
        )
        .add_field(
            "Won?",
            str(won),
        )
        .add_field(
            "Guess",
            str(usr_num),
            inline=True,
        )
        .add_field(
            "Number",
            str(rand_num),
            inline=True,
        )
        .add_field(
            "Range",
            f"{num_range[0]}-{num_range[1] - 1}",
            inline=True,
        )
        .add_field(
            "Message for you",
            message,
        )
        .set_footer(
            text=f"Requested by {user}",
            icon=ctx.member.avatar_url or ctx.member.default_avatar_url,
        )
    )

    await ctx.respond(embed)


def load(bot: lb.BotApp) -> None:  # Takes in lightbulb.BotApp
    bot.add_plugin(fun_plugin)
