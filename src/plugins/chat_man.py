# import hikari as hk
import lightbulb as lb
from log import logging, Logger

chat_plugin = lb.Plugin("ChatManager")
Logger()  # Initialize logging config


class Roles:
    owner = 864131294241357854
    bot = 937143574510833695
    admin = 864132002978856972
    mod = 864142966821486623
    guard = 864132724838105128
    everyone = 864119359940722719


@chat_plugin.command()
@lb.add_cooldown(10.0, 1, lb.UserBucket)  # 10 seconds delay per user usage
@lb.add_checks(lb.has_roles(Roles.owner, Roles.admin, Roles.mod, mode=any))
@lb.option('amount', "The amount of messages to clear.", type=int)
@lb.command('clear', "Clear messages (max 30 a time).")
@lb.implements(lb.SlashCommand)
async def clear_msgs(ctx: lb.Context) -> None:
    user = ctx.author
    amount = ctx.options.amount
    msg_amount = amount if amount <= 30 else 30
    channel = ctx.channel_id

    msgs = await ctx.bot.rest.fetch_messages(channel).limit(msg_amount)
    await ctx.bot.rest.delete_messages(channel, msgs)

    respond_msg = await ctx.respond(f"{len(msgs)} messages has been deleted.")
    await respond_msg.delete()

    if len(msgs) > 10:
        logging.warning(f"{user} successfully cleared {len(msgs)}!")
        return

    logging.info(f"{user} successfully cleared {len(msgs)}!")


@chat_plugin.command()
@lb.add_checks(lb.owner_only, lb.has_roles(Roles.owner))
@lb.command('wipe', "Clears all messages within this channel.")
@lb.implements(lb.SlashCommand)
async def clear_all_msgs(ctx: lb.Context) -> None:
    user = ctx.author

    channel = ctx.channel_id
    msgs = await ctx.bot.rest.fetch_messages(channel)
    await ctx.bot.rest.delete_messages(channel, msgs)

    respond_msg = await ctx.respond("All messages within this channel have been deleted!")
    await respond_msg.delete()
    logging.warning(f"{user} successfully wiped channel: {channel}!")


def load(bot: lb.BotApp) -> None:
    bot.add_plugin(chat_plugin)
