import hikari as hk
import lightbulb as lb
from datetime import datetime
from log import logging, Logger

info_plugin = lb.Plugin("Info")
Logger()  # Initialize logging config
current_time = datetime.now().astimezone()


@info_plugin.command()
@lb.add_cooldown(30.0, 1, lb.UserBucket)  # 30 seconds delay per user usage
@lb.option('user', "Server member to get information about.", type=hk.User)
@lb.command('info', "Pull information on a server member.", ephemeral=True)
@lb.implements(lb.SlashCommand)
async def info(ctx: lb.Context) -> None:
    target = ctx.options.user

    if not target:
        await ctx.respond("Couldn't find the targeted user within the server.")
        logging.debug(f"{target} was unable to be located on the server.")
        return

    created_at = int(target.created_at.timestamp())
    joined_at = int(target.joined_at.timestamp())

    roles = (await target.fetch_roles())[1:]  # All roles but @everyone

    embed = (
        hk.Embed(
            title=f"__User Info__ - {target.display_name}",
            description=f"User ID: `{target.id}`",
            colour=0x00E2E2,
            timestamp=current_time,
        )
        .set_thumbnail(target.avatar_url or target.default_avatar_url)
        .add_field(
            "Bot?",
            str(target.is_bot),
            inline=True,

        )
        .add_field(
            "Acc. cake day",
            f"<t:{created_at}:d>\n*(<t:{created_at}:R>)*",
            inline=True,
        )
        .add_field(
            "Srv. cake day",
            f"<t:{joined_at}:d>\n*(<t:{joined_at}:R>)*",
            inline=True,
        )
        .add_field(
            "Roles",
            ", ".join(r.mention for r in roles),
            inline=False,
        )
        .set_footer(
            text=f"Requested by {ctx.member.display_name}",
            icon=ctx.member.avatar_url or ctx.member.default_avatar_url,
        )
    )

    await ctx.respond(embed)


def load(bot: lb.BotApp) -> None:
    bot.add_plugin(info_plugin)
