import hikari as hk
import lightbulb as lb
from os import environ
from datetime import datetime

from db import *
from log import logging, Logger

gang_plugin = lb.Plugin("Gang")
db = Database("192.168.0.19")  # Connect to db
Logger()  # Initialize logging config
current_time = datetime.now().astimezone()

inv_ch = 944237181541699624
srv_guild = environ["AA_GUILD"]


def is_mention(usr: str) -> False:
    if usr.startswith('<@'):
        return True
    return


# Main gang command
@gang_plugin.command()
@lb.command('gang', "Manage or create your gang.")
@lb.implements(lb.SlashCommandGroup)
async def gang_cmd(ctx: lb.SlashContext) -> None:
    pass


# List all gangs command
@gang_plugin.command()
@lb.add_cooldown(30.0, 1, lb.UserBucket)  # 30 seconds delay per user usage
@lb.command('gangs', "List all gangs on the server.")
@lb.implements(lb.SlashCommand)
async def gangs(ctx: lb.SlashContext) -> None:
    pass


# Gang create command
@gang_cmd.child()
@lb.add_cooldown(30.0, 1, lb.UserBucket)
@lb.option('name', "Enter the name of your intended gang.", type=str)
@lb.command('create', "Create a gang.", ephemeral=True)
@lb.implements(lb.SlashSubCommand)
async def gang_create(ctx: lb.SlashContext) -> None:
    author = ctx.author
    gang_name = ctx.options.name

    try:
        db.gang_create(gang_name, author)
    except GangExistsError:
        await ctx.respond(f"Could not create `{gang_name}` as it already exists! Try again with a different name.")
        logging.debug(f"'{gang_name}' was not created, as it already exists.")
        return
    except AlreadyInGangError:
        await ctx.respond(f"In order to create `{gang_name}` you need to leave your current gang!")
        logging.debug(f"'{gang_name}' was not created, as '{author}' is already in a gang.")
        return

    await ctx.respond(f"Successfully created your gang `{gang_name}`!")
    logging.info(f"'{gang_name}' was created by '{author}'.")

    # async def create_role(gang_name: str, gang_user: str) -> None:
    # await ctx.bot.rest.create_role(guild=srv_guild, name=gang_name, colour=(153, 170, 181))
    # await ctx.bot.rest.add_role_to_member(guild=srv_guild, user=gang_user)


# Gang invite command
@gang_cmd.child()
@lb.add_cooldown(10.0, 2, lb.UserBucket)
@lb.option('person', "Mention whom to invite to your gang.", type=hk.Member)
@lb.command('invite', "Invite someone to your gang.")
@lb.implements(lb.SlashSubCommand)
async def gang_invite(ctx: lb.SlashContext) -> None:
    user = ctx.options.person

    if ctx.author.id == user.id:
        await ctx.respond("You cannot invite yourself to a gang!")
        return

    if not db.is_in_gang(ctx.author):
        await ctx.respond("You must be in a gang in order to invite someone to your gang!")
        return

    if db.get_rank(ctx.author) < Ranks.MOD:
        await ctx.respond("You have insufficient permissions to do so!")
        return

    logging.info(f"{ctx.author} invited {user} to their gang.")

    gang = db.get_gang(ctx.author)
    inv_embed = (
        hk.Embed(
            title=f"__Gang Invitation__ - {gang}",
            description=f"{user.mention} you have recieved an invitation to join '{gang}' from {ctx.member.mention}!",
            colour=0x00E2E2,
            timestamp=current_time
        )
        .add_field(
            'Accept',
            "To accept the invite press the green checkmark reaction.",
            inline=True,
        )
        .add_field(
            'Decline',
            "In order to decline the invite press the red cross reaction.",
            inline=True,
        )
        .set_footer(
            text=f"Requested by {ctx.member.display_name}",
            icon=ctx.member.avatar_url or ctx.member.default_avatar_url,
        )
    )

    msg = await ctx.bot.rest.create_message(inv_ch, inv_embed)
    await msg.add_reaction("✅")  # Use buttons instead of reactions (miru framework)
    await msg.add_reaction("❌")
    await ctx.respond(f"**`{user}`** has recieved your invitation!")


@gang_cmd.child()
@lb.add_cooldown(10.0, 1, lb.UserBucket)
@lb.command('members', "Find out who is currently in your gang.")
@lb.implements(lb.SlashSubCommand)
async def gang_members(ctx: lb.SlashContext) -> None:
    try:
        member_ids = db.get_gang_members(user=ctx.author)
        members = [f"<@{m}>" for m in member_ids]
        await ctx.respond(f"Members: {members}")

    except EmptyResponseError as err:
        await ctx.respond("Couldn't fetch your gang members, make sure you're in a gang!")
        logging.warning(err)


@gang_cmd.child()
@lb.add_cooldown(3.0, 1, lb.UserBucket)
@lb.option('member', "Mention whom to kick.", type=hk.Member)
@lb.command('kick', "Kick a member from your gang.", ephemeral=True)
@lb.implements(lb.SlashSubCommand)
async def gang_kick(ctx: lb.SlashContext) -> None:
    author = ctx.author
    subject = ctx.options.member

    if not db.is_in_gang(subject):
        await ctx.respond("You cannot leave a gang whilst not being in any gang!")
        logging.debug(f"'{author}' tried leaving a gang they ain't part of.")
        return

    if not db.is_in_same_gang(author, subject):
        await ctx.respond("Nice try! You cannot kick someone that isn't in your gang...")
        logging.debug(f"'{author}' cannot kick '{subject}', as they're in the same gang.")
        return

    if not db.has_permission(author, subject):
        await ctx.respond(f"You have insufficient permissions to kick `{subject.mention}`!")
        logging.debug(f"'{author}' lacks permission to kick '{subject}'.")
        return

    if int(author.id) == int(subject.id):
        await ctx.respond("If you wish to disband your gang, do so by using the `/gang leave` command.")
        return

    db.gang_kick(subject)
    await ctx.respond(f"{subject.mention} was successfully kicked from your gang!")


@gang_cmd.child()
@lb.add_cooldown(30.0, 1, lb.UserBucket)
@lb.command('leave', "Leave your gang.", ephemeral=True)
@lb.implements(lb.SlashSubCommand)
async def gang_leave(ctx: lb.SlashContext) -> None:
    # If gang leader tries to leave the gang, return an embed asking if they wish to pass on the leadership.
    # Notify other gang members in an ephemeral way if possible if gang is disbanded
    user = ctx.author

    # Checks if the author even is in a gang
    if not db.is_in_gang(user):
        await ctx.respond("You cannot leave a gang whilst not being in any gang!")
        return

    # Checks if the author is the gang leader
    if db.is_gang_leader(user):
        gang = db.get_gang(user)
        db.gang_delete(gang)
    else:
        db.gang_kick(user)

    await ctx.respond("You have successfully parted from your gang!")


@gang_cmd.child()
@lb.add_cooldown(5.0, 2, lb.UserBucket)
@lb.option('member', "Mention whom to promote.", type=hk.Member)
@lb.command('promote', "Promote a member in your gang.", ephemeral=True)
@lb.implements(lb.SlashSubCommand)
async def gang_promote(ctx: lb.SlashContext) -> None:
    pass


@gang_cmd.child()
@lb.add_cooldown(60.0, 1, lb.UserBucket)
@lb.option('member', "Mention whom to make the new gang leader.", type=hk.Member)
@lb.command('leader', "Pass on the leadership to another member in your gang.", ephemeral=True)
@lb.implements(lb.SlashSubCommand)
async def gang_leader(ctx) -> None:
    # Notify the gang chat channel
    pass


@gang_cmd.child()
@lb.add_cooldown(5.0, 1, lb.UserBucket)
@lb.option('member', "Mention whom to manage.", type=hk.Member)
@lb.command('manage', "Easily manage a member's rank & etc.", ephemeral=True)
@lb.implements(lb.SlashSubCommand)
async def gang_manage(ctx: lb.SlashContext) -> None:
    # Embed with reactions representing different actions to apply upon a person
    pass


@gang_plugin.command()
@lb.add_checks(lb.owner_only, lb.has_roles(864131294241357854))
@lb.command('test', 'Testing stuff', ephemeral=True)
@lb.implements(lb.SlashCommand)
async def testing(ctx) -> None:
    print(f"\nList of gangs:\n{db.gangs}")
    print(f"\nIs in a gang?\n{db.is_in_gang(ctx.author)}")
    print(f"\nHas permission:\n{db.has_permission(ctx.author, ctx.author)}")
    print(f"\nIs gang leader?\n{db.is_gang_leader(ctx.author)}")
    print(f"\nGet author's gang:\n{db.get_gang(ctx.author)}")
    print(f"\nAuthor's gang listed:\n{db.gang_list('Happy Meal')}")
    print(f"\nIs in same gang?\n{db.is_in_same_gang(ctx.author, ctx.author)}")
    await ctx.respond("Check ur terminal")


# Loads the plugin when the bot starts
def load(bot: lb.BotApp) -> None:
    bot.add_plugin(gang_plugin)
