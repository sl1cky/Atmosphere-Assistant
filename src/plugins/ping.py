import lightbulb as lb
from log import Logger, logging

ping_plugin = lb.Plugin("Ping")
Logger()  # Initialize logging config


@ping_plugin.command()
@lb.add_cooldown(30.0, 1, lb.UserBucket)  # 30 seconds delay per user usage
@lb.command('ping', "Ping pong, what's my latency?")
@lb.implements(lb.SlashCommand)
async def ping(ctx: lb.SlashContext) -> None:
    latency = ctx.bot.heartbeat_latency
    await ctx.respond(f"Pong! - Your ping is `{latency:.2f}ms`")
    logging.debug(f"{ctx.author}'s ping is {latency:.2f}ms")


def load(bot: lb.BotApp) -> None:
    bot.add_plugin(ping_plugin)
