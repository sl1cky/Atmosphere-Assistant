import os
import uvloop
import hikari as hk
import lightbulb as lb
from log import logging, Logger


Logger()  # Initialize logging config
token = os.environ["AA_TOKEN"]
srv_guild = int(os.environ["AA_GUILD"])
owner_id = os.environ["AA_OWNER"]

bot = lb.BotApp(
    token.strip(),
    owner_ids=owner_id,
    default_enabled_guilds=(srv_guild),
    help_slash_command=True,
    # intents=hk.Intents.ALL,  # Makes sure the bot has all its required perms
)

bot.load_extensions_from('./plugins/', must_exist=True)  # Loads all the plugins aka the custom commands


@bot.listen(hk.StartedEvent)
async def on_ready(event):
    print("Atmosphere Assistant has succesfully booted and is ready to rock and roll!")
    logging.info("Atmosphere Assistant is ready to rock n roll!")


@bot.listen(hk.StoppingEvent)
async def on_stopping(event) -> None:
    logging.info("Atmosphere Assistant shutting down!")


@bot.listen(hk.StoppedEvent)
async def on_stopped(event) -> None:
    print("Atmosphere Assistant has succesfully shut down!")
    logging.info("Atmosphere Assistant has succesfully shut down!")


# Log message events
@bot.listen(hk.GuildMessageCreateEvent)
async def print_msg(event) -> None:
    if event.author.is_bot:
        return
    logging.info(f"CH: {event.guild_id} USR: {event.author} MSG: {event.content}")


# Error event handler
@bot.listen(lb.CommandErrorEvent)
async def err_handler(event: lb.CommandErrorEvent) -> None:
    ctx = event.context
    err = event.exception.__cause__ or event.exception  # __cause__ to get the original exception's cause

    # If not bot owner
    if isinstance(err, lb.errors.NotOwner):
        await ctx.respond("This command can only be used by the bot's maker!", flags=hk.MessageFlag.EPHEMERAL)

    # If command is on cooldown
    if isinstance(err, lb.errors.CommandIsOnCooldown):
        await ctx.respond(f"This command is on cooldown for another {err.retry_after:.2f} seconds!",
                          flags=hk.MessageFlag.EPHEMERAL)

    # Insufficient role or permission handling
    if isinstance(err, lb.errors.MissingRequiredRole or lb.errors.MissingRequiredPermission):
        await ctx.respond("You are not permitted to use this command!", flags=hk.MessageFlag.EPHEMERAL)
        logging.warning(f"Insufficient role or permission for '{ctx.member}'.")

    # Handling of non-server calls
    if isinstance(err, lb.errors.OnlyInGuild):
        await ctx.respond("This command can only be used in servers!", flags=hk.MessageFlag.EPHEMERAL)
        logging.debug(f"The following command '{ctx.invoked}' was invoked by '{ctx.member}' and raised error '{err}'")

    # DMs only command calls handling
    if isinstance(err, lb.errors.OnlyInDM):
        await ctx.respond("This command can only be used in DMs!", flags=hk.MessageFlag.EPHEMERAL)
        logging.debug(f"The following command '{ctx.invoked}' was invoked by '{ctx.member}' and raised error '{err}'")


if __name__ == '__main__':
    # Uvloop allows for some extra performance boost
    if os.name != 'nt':  # On windows uvloop doesn't work
        uvloop.install()

    # Have the bot run
    bot.run()
