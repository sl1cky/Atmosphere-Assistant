import logging


class Logger:
    # Logger class with the purpose of setting my desired logging config when called.
    def __init__(
        self,
        filename="aa-bot.log",
        format_="%(asctime)s | %(levelname)s - %(message)s",
        level=logging.DEBUG,
        filemode="a",
    ) -> logging.basicConfig:

        self.file = str(filename)
        self.format = str(format_)
        self.level = level
        self.filemode = str(filemode)
        self.log_conf = logging.basicConfig(
            filename=self.file,
            format=self.format,
            level=self.level,
            filemode=self.filemode,
        )

        return self.log_conf

    def __str__(self):
        print(
            f"Filename: '{self.file}'\nLogging format: '{self.format}'"
            f"\nLogging level: '{self.level}'\nFile logging mode: '{self.filemode}'"
        )
