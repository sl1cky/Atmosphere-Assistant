from re import sub
from os import environ
import psycopg2 as psy
from hikari import Member
from db.errors import *
from datetime import datetime


class Ranks:
    LEADER = 10
    ADMIN = 20
    MOD = 30
    MEMBER = 50


class Database:
    def __init__(
        self,
        host="asphere.duckdns.org",
        db="aa_dbot_gangs",
        port="5432",
        user="discord",
        psk=environ["AA_DB_PSK"],
    ) -> None:

        self.host = host
        self.db = db
        self.port = port
        self.user = user
        self.psk = psk

    def __run(self, query: str, fetch: bool = False) -> list | None:
        """Runs a SQL command and optionally fetches the response e.g. if the query is an IMPORT command.

        Args:
            query (str): The SQL query to be run.
            fetch (bool, optional): Fetches a response if True. Defaults to False.

        Raises:
            EmptyResponseError: Raised by an empty response from the db.

        Returns:
            list | None: A list containing the response or None.
        """
        with psy.connect(
            host=self.host, dbname=self.db, user=self.user, password=self.psk
        ) as self.pgsql, self.pgsql.cursor() as self.cursor:
            self.cursor.execute(query)

            if not fetch:
                return None

            data = self.cursor.fetchall()
            if len(data) < 1:
                raise EmptyResponseError("Response from db does not contain any data!")

            return data

    def gang_create(self, gang: str, author: Member):
        """SQL instructions to create a gang table.

        Raises:
            AlreadyInGangError: Raised if author was found in an existing gang.
            GangExistsError: Raised if specified gang already exists.

        Args:
            gang (str): Both the name for a gang and the table.
            author (Member): The user whom ran the gang create command.
        """
        self.time = datetime.now().strftime("%H:%M:%S %d-%m-%Y")
        # Insert author into the table of users along with the other attributes
        try:
            self.__run(
                f"""
                INSERT INTO "users" ("id", "gang_name", "gang_rank", "gang_jdate") VALUES
                ('{author.id}', '{gang}', '10', '{self.time}');
                """
            )
        except psy.errors.UniqueViolation:
            raise AlreadyInGangError(f"'{author.id}' is already in a gang")
        # Insert newly created gang into the table of gangs
        try:
            self.__run(
                f"""
                INSERT INTO "gangs" ("gang", "creator_id", "c_date") VALUES
                ('{gang}', '{author.id}', '{self.time}');
                """
            )
        except psy.errors.UniqueViolation:
            raise GangExistsError(
                f"Could not create '{gang}' as it already exists in the gang table."
            )

    def gang_delete(self, gang: str):
        """SQL instructions for deleting a gang table.

        Args:
            gang (str): Table to drop.
        """
        self.__run(
            f"""DELETE FROM "gangs" WHERE "gang" = '{gang}';
                DELETE FROM "users" WHERE "gang_name" = '{gang}';"""
        )

    @property
    def gangs(self) -> list:
        """SQL instructions for listing every gang.

        Returns:
            list: Response containg a list of all the tables.
        """
        return self.__run("""SELECT "gang" from "gangs";""", True)

    def gang_list(self, gang: str) -> list:
        """SQL instructions for listing the contents of a given gang.

        Args:
            gang (str): Gang to retrieve info from.

        Returns:
            list: Response from the db, containing all info regarding the users of a gang.
        """
        return self.__run(
            f"""SELECT * FROM "users" WHERE "gang_name" = '{gang}';""", True
        )

    def gang_join(self, gang: str, member: Member):
        """SQL instructions for appending a user to a table.

        Args:
            gang (str): Table name to append to.
            member (Member): User to append.
        """
        self.__run(
            f"""
            INSERT INTO "users" ("id", "gang_name", "gang_rank", "gang_jdate")
            VALUES ('{member.id}', '{gang}', '{Ranks.MEMBER}', '{self.time}');
            """
        )

    def gang_kick(self, member: Member):
        """SQL instructions for removing a user from a table.

        Args:
            gang (str): Table name to erase from.
            member (Member): User to erase.
        """
        self.__run(f"""DELETE FROM "users" WHERE usr_id = '{member.id}';""")

    def gang_manage(self, member: Member, rank: int = 30):
        """SQL instructions for managing a member's rank.

        Args:
            member (Member): Member to modify.
            rank (int, optional): Member's rank to promote/demote to. Defaults to 30 (MOD).
        """
        self.__run(
            f"""UPDATE "users" SET "gang_rank" = '{rank}' WHERE "id" = '{member.id}';"""
        )

    def has_permission(self, person1: Member, person2: Member) -> bool:
        """Checks if person1 has authority over person2.

        Args:
            person1 (Member): First person to match against the second person.
            person2 (Member): Second person to mathc against the first person.

        Returns:
            bool: True | False.
        """
        p1 = self.__run(
            f"""SELECT "gang_rank" FROM "users" WHERE "id" = '{person1.id}';""", True
        )[0][0]
        p2 = self.__run(
            f"""SELECT "gang_rank" FROM "users" WHERE "id" = '{person2.id}';""", True
        )[0][0]

        if int(p1) >= int(p2):
            return True
        return False

    def get_gang(self, person: Member) -> str:
        """Fetches the gang a specified person is part of.

        Args:
            person (Member): User to check against in search of a gang.

        Returns:
            str | None: Name of gang returned as a string or NoneType if no gang was found.
        """
        gang = self.__run(
            f"""SELECT "gang_name" FROM "users" WHERE "id" = '{person.id}';""", True
        )
        return gang[0][0]  # Get the first object from the list and the first item in the tuple

    def get_gang_members(self, gang: str = None, user: Member = None) -> list:
        """Get a list of tuples with members in a certain gang specified either by a gang name or a user.

        Args:
            gang (str, optional): Gang to fetch from
            user (Member, optional): Person to fetch gang members from. Defaults to None.

        Returns:
            tuple: Tuple containing fetched db response.
        """
        if user is not None:
            gang = self.get_gang(user)
        members_tup = self.__run(
            f"""SELECT "id" FROM "users" WHERE "gang_name" = '{gang}';""", True
        )
        # Remove the tuples from the list:
        members = [sub(r'\(|,\)', '', str(x)) for x in members_tup]
        return members

    def is_in_gang(self, person: Member) -> bool:
        """Checks if a given user is in a gang or not.

        Args:
            member (Member): User to check against.

        Returns:
            bool: True | False.
        """
        try:
            self.__run(
                f"""SELECT * FROM "users" WHERE "id" = '{person.id}';""", fetch=True
            )[0][0]
            return True
        except EmptyResponseError:
            return False

    def is_gang_leader(self, person: Member) -> bool:
        """Checks if a given user is the leader of any gang.

        Args:
            person (Member): User to check against.

        Returns:
            bool: True | False.
        """
        try:
            self.__run(
                f"""SELECT ("id", "gang_rank") FROM "users" WHERE
                "id" = '{person.id}' AND "gang_rank" = '{Ranks.LEADER}';""",
                True,
            )[0][0]
            return True
        except EmptyResponseError:
            return False

    def is_in_same_gang(self, *people: Member) -> bool:
        gangs = []
        # Append every person's gang to the 'gangs' list
        for person in people:
            # If a person isn't in a gang, they obviously aren't in the same gang
            if not self.is_in_gang(person):
                return False
            gangs.append(
                self.__run(
                    f"""SELECT "gang_name" FROM "users" WHERE "id" = '{person.id}';""",
                    True,
                )
            )
        # Checks if the next gang matches the first gang in the list
        for gang in gangs:
            if gang[0][0] != gangs[0][0][0]:
                return False
        return True
