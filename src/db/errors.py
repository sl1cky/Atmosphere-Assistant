class EmptyResponseError(Exception):
    pass


class GangExistsError(Exception):
    pass


class AlreadyInGangError(Exception):
    pass
